import matplotlib.pyplot as plt 

#Strings referentes às variáveis buscadas no log
s_inicio_loop, s_fim_loop = "deltaT =", "ExecutionTime"
s_contErrP1, s_contErrP2 = "contErrP1", "contErrP2"
s_rho1, s_rho2 = "rho1", "rho2"
s_dgdt = "dgdt"
s_PIMPLE, s_GAMGPCG, s_smoothSolver = "PIMPLE", "GAMGPCG", "smoothSolver"

def main():

    log_name = input("Diretório do arquivo de log: ")

    try:
        log_file = open(log_name, 'r')
    except:
        print("Erro ao abrir o arquivo desejado")
        return

    #Inicialização das listas
    deltaT, time = [], []
    contErrP1max, contErrP1min, contErrP2max, contErrP2min = [], [], [], []
    rho1min, rho1max, rho2min, rho2max = [], [], [], []
    dgdtmin, dgdtmax = [], []
    PIMPLE = []
    p_rgh, S = [], []
    total_iter_S, total_iter_p = [], []

    S_type = ''

    #Strings que armazenarão linhas contendo informações retiradas do log

    curr_cEP1_line, curr_cEP2_line = '', ''
    curr_rho1_line, curr_rho2_line = '', ''
    curr_dgdt_line = ''
    curr_PIMPLE_line = ''
    curr_smoothSolver_line, curr_GAMGPCG_line = '', ''

    #Variáveis para controle dos inicios de loop

    inicio_loop_g = inicio_loop_s = True

    #Variáveis para extrair valores do log

    forma_log = determina_formato_log(log_file)

    if (forma_log[0]):
        pos_cEP1_min, pos_cEP1_max, (cEP1_i_min, cEP1_j_min), (cEP1_i_max, cEP1_j_max) = forma_log[0]

    if (forma_log[1]):
        pos_cEP2_min, pos_cEP2_max, (cEP2_i_min, cEP2_j_min), (cEP2_i_max, cEP2_j_max) = forma_log[1]

    if (forma_log[2]):
        pos_rho1_min, pos_rho1_max, (rho1_i_min, rho1_j_min), (rho1_i_max, rho1_j_max) = forma_log[2]

    if (forma_log[3]):
        pos_rho2_min, pos_rho2_max, (rho2_i_min, rho2_j_min), (rho2_i_max, rho2_j_max) = forma_log[3]

    if (forma_log[4]):
        pos_dgdt_min, pos_dgdt_max, (dgdt_i_min, dgdt_j_min), (dgdt_i_max, dgdt_j_max) = forma_log[4]

    if (forma_log[5]):
        pos_PIMPLE = forma_log[5]

    if (forma_log[6]):
        pos_p_rgh, (p_rgh_i, p_rgh_j) = forma_log[6]

    if (forma_log[7]):
        S_type, pos_S, (S_i, S_j)  = forma_log[7]


    log_file.seek(0)

    for line in log_file:
        if (line[0 : len(s_inicio_loop)] == s_inicio_loop):
            
            currline_list = line.split(' ')
            deltaT.append(float(currline_list[-1]))

            currline_list = log_file.readline().split(' ')
            time.append(float(currline_list[-1]))

            currline = log_file.readline()
            iter_S_loop = 0
            iter_p_loop = 0

            while (currline[0 : len(s_fim_loop)] != s_fim_loop and currline):

                if (currline[0:len(s_contErrP1)] == s_contErrP1):
                    curr_cEP1_line = currline
                
                elif (currline[0:len(s_contErrP2)] == s_contErrP2):
                    curr_cEP2_line = currline
                
                elif (currline[0:len(s_rho1)] == s_rho1):
                    curr_rho1_line = currline

                elif (currline[0:len(s_rho2)] == s_rho2):
                    curr_rho2_line = currline

                elif (currline[0:len(s_dgdt)] == s_dgdt):
                    curr_dgdt_line = currline

                elif (currline[0:len(s_PIMPLE)] == s_PIMPLE):
                    curr_PIMPLE_line = currline
                    inicio_loop_g = True
                    inicio_loop_s = True

                elif (currline[0:len(s_GAMGPCG)] == s_GAMGPCG):
                    iter_p_loop += le_total_iteracoes(currline)
                    if(inicio_loop_g):
                        curr_GAMGPCG_line = currline
                        inicio_loop_g = False

                elif (currline[0:len(s_smoothSolver)] == s_smoothSolver):
                    iter_S_loop += le_total_iteracoes(currline)
                    if(inicio_loop_s):
                        curr_smoothSolver_line = currline
                        inicio_loop_s = False

                currline = log_file.readline()

            if (not currline):
                break

            if (curr_cEP1_line):
                curr_cEP1_line = curr_cEP1_line.split(' ')
                contErrP1min.append(float(curr_cEP1_line[pos_cEP1_min][cEP1_i_min:cEP1_j_min]))
                contErrP1max.append(float(curr_cEP1_line[pos_cEP1_max][cEP1_i_max:cEP1_j_max]))
            
            if (curr_cEP2_line):
                curr_cEP2_line = curr_cEP2_line.split(' ')
                contErrP2min.append(float(curr_cEP2_line[pos_cEP2_min][cEP2_i_min:cEP2_j_min]))
                contErrP2max.append(float(curr_cEP2_line[pos_cEP2_max][cEP2_i_max:cEP2_j_max]))

            if (curr_rho1_line):
                curr_rho1_line = curr_rho1_line.split(' ')
                rho1min.append(float(curr_rho1_line[pos_rho1_min][rho1_i_min:rho1_j_min]))
                rho1max.append(float(curr_rho1_line[pos_rho1_max][rho1_i_max:rho1_j_max]))

            if (curr_rho2_line):
                curr_rho2_line = curr_rho2_line.split(' ')
                rho2min.append(float(curr_rho2_line[pos_rho2_min][rho2_i_min:rho2_j_min]))
                rho2max.append(float(curr_rho2_line[pos_rho2_max][rho2_i_max:rho2_j_max]))

            if (curr_dgdt_line):
                curr_dgdt_line = curr_dgdt_line.split(' ')
                dgdtmin.append(float(curr_dgdt_line[pos_dgdt_min][dgdt_i_min:dgdt_j_min]))
                dgdtmax.append(float(curr_dgdt_line[pos_dgdt_max][dgdt_i_max:dgdt_j_max]))

            if (curr_PIMPLE_line):
                curr_PIMPLE_line = curr_PIMPLE_line.split(' ')
                PIMPLE.append(int(curr_PIMPLE_line[pos_PIMPLE]))

            if (curr_GAMGPCG_line):
                curr_GAMGPCG_line = curr_GAMGPCG_line.split(' ')
                p_rgh.append(float(curr_GAMGPCG_line[pos_p_rgh][p_rgh_i:p_rgh_j]))

            if (curr_smoothSolver_line):
                curr_smoothSolver_line = curr_smoothSolver_line.split(' ')
                S.append(float(curr_smoothSolver_line[pos_S][S_i:S_j]))

            if (iter_p_loop > 0):
                total_iter_p.append(iter_p_loop)
            if (iter_S_loop > 0):    
                total_iter_S.append(iter_S_loop)


    log_file.close()

    if (len(time) > len(PIMPLE)):
        time.pop()
        deltaT.pop()

    salvar_g = input("Deseja salvar os gráficos em arquivos png? (S)im, (N)ão: ").lower()

    while (salvar_g != 's' and salvar_g != 'n'):
        print("Entrada inválida")
        salvar_g = input("Deseja salvar os gráficos em arquivos png? (S)im, (N)ão: ").lower()

    if (salvar_g == 's'):
        dir_g = input("Diretório onde salvar os gráficos: ")
        if (dir_g[-1] != '/'):
            dir_g = dir_g + '/'
    else:
        dir_g = ''

    gera_grafico1(p_rgh, S, S_type, time, salvar_g, dir_g)
    gera_grafico2(total_iter_p, total_iter_S, S_type, time, salvar_g, dir_g)
    gera_grafico3(PIMPLE, time, salvar_g, dir_g)
    gera_graficos4(contErrP1max, contErrP1min, contErrP2max, contErrP2min, rho1max, rho1min,
    rho2max, rho2min, dgdtmax, dgdtmin, time, salvar_g, dir_g)
    gera_grafico5(deltaT, time, salvar_g, dir_g)



#A função buscará pelas palavras "min" e "max" e extrairá os valores após elas
def formato_string_min_max(linha):

    if (not linha):
        return
    
    lista_linha = linha.split(' ')

    pos_min = 0
    tam = len(lista_linha)

    while (pos_min < tam and lista_linha[pos_min].find("min") == -1):
        pos_min += 1

    pos_max = 0
    while (pos_max < tam and lista_linha[pos_max].find("max") == -1):
        pos_max += 1

    if (pos_min < tam):
        while (pos_min < tam and not any(chr.isdigit() for chr in lista_linha[pos_min])):
            pos_min += 1
        if (pos_min < tam):
            i_min, j_min = extrair_pos_num_string(lista_linha[pos_min])

    if (pos_max < tam):
        while (pos_max < tam and not any(chr.isdigit() for chr in lista_linha[pos_max])):
            pos_max += 1
        if (pos_max < tam):
            i_max, j_max = extrair_pos_num_string(lista_linha[pos_max])

    #Aqui, temos pos_min e pos_max como posições dos valores min e max

    return pos_min, pos_max, (i_min, j_min), (i_max, j_max) 


def formato_linha_PIMPLE(linha):

    if (not linha):
        return

    lista_linha = linha.split(' ')

    i = -1

    while (i >= (-len(lista_linha)) and not lista_linha[i].isdigit()):
        i -= 1
    
    if (i >= (-len(lista_linha))):
        return i



def formato_linha_GAMGPCG(linha):

    if (not linha):
        return

    lista_linha = linha.split(' ')

    i_initial = 0
    tam = len(lista_linha)

    while (i_initial < tam and lista_linha[i_initial] == "Initial"):
        i_initial += 1

    while (i_initial < tam and not any(chr.isdigit() for chr in lista_linha[i_initial])):
        i_initial += 1

    if (i_initial < tam):
        i, j = extrair_pos_num_string(lista_linha[i_initial])

    return i_initial, (i, j)
    

def formato_linha_smoothSolver(linha):

    if (not linha):
        return


    lista_linha = linha.split(' ')

    i_initial = 0
    tam = len(lista_linha)

    while (i_initial < tam and lista_linha[i_initial] == "Initial"):
        i_initial += 1

    while (i_initial < tam and not any(chr.isdigit() for chr in lista_linha[i_initial])):
        i_initial += 1

    if (i_initial < tam):
        i, j = extrair_pos_num_string(lista_linha[i_initial])


    for word in lista_linha:
        if (word[0:2] == "S."):
            if ('z' > word[-1] and word[-1] > 'a'):
                S_type = word
            else:
                S_type = word[:-1]
            break

    return S_type, i_initial, (i, j)

def le_total_iteracoes(linha):

    lista_linha = linha.split(' ')

    #Não contar o \n ao fim da linha
    if (lista_linha[-1][:-1].isdigit() and lista_linha[-2] == "Iterations"):
        return int(lista_linha[-1])
    
    return 0

#Devolve indice do começo do número na string e indice do fim (fim -> contando de trás pra frente) 
def extrair_pos_num_string(string):

    i = 0

    while (string[i] != '-' and not string[i].isdigit()):
        i += 1

    j = len(string) - 1

    while (not string[j].isdigit()):
        j -= 1
    
    if (j - (len(string) - 1) < 0):
        return i, (j - (len(string) - 1))
    else:
        return (i, None)



#Função que determinará forma do log, achando posições dos números para obter leitura mais rápida
def determina_formato_log(log_file):

    curr_cEP1_line, curr_cEP2_line = '', ''
    curr_rho1_line, curr_rho2_line = '', ''
    curr_dgdt_line = ''
    curr_PIMPLE_line = ''
    curr_smoothSolver_line, curr_GAMGPCG_line = '', ''
    
    for line in log_file:
        if (line[0 : len(s_inicio_loop)] == s_inicio_loop):
            
            currline = log_file.readline()

            while (currline[0 : len(s_fim_loop)] != s_fim_loop and currline):

                if (currline[0:len(s_contErrP1)] == s_contErrP1):
                    curr_cEP1_line = currline
                
                elif (currline[0:len(s_contErrP2)] == s_contErrP2):
                    curr_cEP2_line = currline
                
                elif (currline[0:len(s_rho1)] == s_rho1):
                    curr_rho1_line = currline

                elif (currline[0:len(s_rho2)] == s_rho2):
                    curr_rho2_line = currline

                elif (currline[0:len(s_dgdt)] == s_dgdt):
                    curr_dgdt_line = currline  

                elif (currline[0:len(s_PIMPLE)] == s_PIMPLE):
                    curr_PIMPLE_line = currline

                elif (currline[0:len(s_GAMGPCG)] == s_GAMGPCG):
                    curr_GAMGPCG_line = currline

                elif (currline[0:len(s_smoothSolver)] == s_smoothSolver):
                    curr_smoothSolver_line = currline

                currline = log_file.readline()
            
            if (curr_cEP1_line):
                dados_cEP1 = formato_string_min_max(curr_cEP1_line)
            else:
                dados_cEP1 = ''

            if (curr_cEP2_line):
                dados_cEP2 = formato_string_min_max(curr_cEP2_line)
            else:
                dados_cEP2 = ''

            if (curr_rho1_line):
                dados_rho1 = formato_string_min_max(curr_rho1_line)
            else:
                dados_rho1 = ''

            if (curr_rho2_line):
                dados_rho2 = formato_string_min_max(curr_rho2_line)
            else:
                dados_rho2 = ''

            if (curr_dgdt_line):
                dados_dgdt = formato_string_min_max(curr_dgdt_line)
            else:
                dados_dgdt = ''

            if (curr_PIMPLE_line):
                pos_PIMPLE = formato_linha_PIMPLE(curr_PIMPLE_line)
            else:
                pos_PIMPLE = '' 

            if (curr_GAMGPCG_line):
                dados_p_rgh = formato_linha_GAMGPCG(curr_GAMGPCG_line)
            else:
                dados_p_rgh = ''

            if(curr_smoothSolver_line):
                dados_S = formato_linha_smoothSolver(curr_smoothSolver_line)
            else:
                dados_S = ''

            break

    return((dados_cEP1, dados_cEP2, dados_rho1, dados_rho2, 
    dados_dgdt, pos_PIMPLE, dados_p_rgh, dados_S))

def gera_grafico1(p_rgh, S, S_type, time, s, dir):

    if (len(p_rgh) == 0 and len(S) == 0):
        return

    plt.clf()
    if (len(p_rgh) > 0):
        plt.plot(time, p_rgh, label="p_rgh", linewidth="0.8")
    if (len(S) > 0):
        plt.plot(time, S, label=S_type, linewidth="0.8")
    plt.xlabel("Time [s]")
    plt.ylabel("Initial residual")
    plt.title("Residuals")
    plt.legend()
    plt.grid("on")
    if (s == 'n'):
        plt.show()
    else:
        plt.savefig(dir + "Initial_residual.png")

def gera_grafico2(total_iter_p, total_iter_S, S_type, time, s, dir):

    if (len(total_iter_p) == 0 and len(total_iter_S) == 0):
        return

    plt.clf()
    if (len(total_iter_p) > 0):
        plt.plot(time, total_iter_p, label="p_rgh", linewidth="0.8")
    if (len(total_iter_S) > 0):
        plt.plot(time, total_iter_S, label=S_type, linewidth="0.8")
    plt.xlabel("Time [s]")
    plt.ylabel("No. of Iterations")
    plt.title("Iterations")
    plt.legend()
    plt.grid("on")
    if (s == 'n'):
        plt.show()
    else:
        plt.savefig(dir + "N_Iterations.png")

def gera_grafico3(PIMPLE, time, s, dir):

    if (len(PIMPLE) == 0):
        return

    plt.clf()
    plt.plot(time, PIMPLE, label="PIMPLE", linewidth="0.8")
    plt.xlabel("Time [s]")
    plt.ylabel("Max Iterations")
    plt.title("Iterations")
    plt.legend()
    plt.grid("on")
    if (s == 'n'):
        plt.show()
    else:
        plt.savefig(dir + "Max_Iterations.png")

def gera_graficos4(c1max, c1min, c2max, c2min, r1max, r1min, r2max, r2min, dgmax, dgmin, t, s, dir):

    if (len(c1max) > 0 and len(c1min) > 0): 
        plt.clf()
        plt.plot(t, c1max, label="contErrP1max", linewidth="0.8")
        plt.plot(t, c1min, label="contErrP1min", linewidth="0.8")
        plt.xlabel("Time [s]")
        plt.ylabel("contErrP1")
        plt.title("Erro")
        plt.legend()
        plt.grid("on")
        if (s == 'n'):
            plt.show()
        else:
            plt.savefig(dir + "contErrP1.png")

    if (len(c2max) > 0 and len(c2min) > 0):
        plt.clf()
        plt.plot(t, c2max, label="contErrP2max", linewidth="0.8")
        plt.plot(t, c2min, label="contErrP2min", linewidth="0.8")
        plt.xlabel("Time [s]")
        plt.ylabel("contErrP2")
        plt.title("Erro")
        plt.legend()
        plt.grid("on")
        if (s == 'n'):
            plt.show()
        else:
            plt.savefig(dir + "contErrP2.png")

    if (len(r1max) > 0 and len(r1min) > 0):
        plt.clf()
        plt.plot(t, r1max, label="rho1max", linewidth="0.8")
        plt.plot(t, r1min, label="rho1min", linewidth="0.8")
        plt.xlabel("Time [s]")
        plt.ylabel("rho1")
        plt.title("Rho")
        plt.legend()
        plt.grid("on")
        if (s == 'n'):
            plt.show()
        else:
            plt.savefig(dir + "rho1.png")

    if (len(r2max) > 0 and len(r2min) > 0):
        plt.clf()
        plt.plot(t, r2max, label="rho2max", linewidth="0.8")
        plt.plot(t, r2min, label="rho2min", linewidth="0.8")
        plt.xlabel("Time [s]")
        plt.ylabel("rho2")
        plt.title("Rho")
        plt.legend()
        plt.grid("on")
        if (s == 'n'):
            plt.show()
        else:
            plt.savefig(dir + "rho2.png")

    if (len(dgmax) > 0 and len(dgmin) > 0):
        plt.clf()
        plt.plot(t, dgmax, label="dgdtmax", linewidth="0.8")
        plt.plot(t, dgmin, label="dgdtmin", linewidth="0.8")
        plt.xlabel("Time [s]")
        plt.ylabel("dgdt")
        plt.title("DgDt")
        plt.legend()
        plt.grid("on")
        if (s == 'n'):
            plt.show()
        else:
            plt.savefig(dir + "dgdt.png")

def gera_grafico5(deltaT, time, s, dir):
    
    if (len(deltaT) == 0):
        return

    plt.clf()
    plt.plot(time, deltaT, linewidth="0.8")
    plt.xlabel("Time [s]")
    plt.ylabel("deltaT [s]")
    plt.title("Time")
    plt.grid("on")
    if (s == 'n'):
        plt.show()
    else:
        plt.savefig(dir + "deltaT.png")


main()