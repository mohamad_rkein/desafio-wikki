import matplotlib.pyplot as plt 

def main():

    log_name = input("Diretório do arquivo de log: ")

    try:
        log_file = open(log_name, 'r')
    except:
        print("Erro ao abrir o arquivo desejado")
        return

    S_type = ''

    #Strings referentes às variáveis buscadas no log
    s_inicio_loop, s_fim_loop = "deltaT =", "ExecutionTime"
    s_contErrP1, s_contErrP2 = "contErrP1", "contErrP2"
    s_rho1, s_rho2 = "rho1", "rho2"
    s_dgdt = "dgdt"
    s_PIMPLE, s_GAMGPCG, s_smoothSolver = "PIMPLE", "GAMGPCG", "smoothSolver"

    #Tamanho das strings

    l_s_inicio_loop, l_s_fim_loop = len(s_inicio_loop), len(s_fim_loop)
    l_s_contErrP1, l_s_contErrP2 = len(s_contErrP1), len(s_contErrP2)
    l_s_rho1, l_s_rho2 = len(s_rho1), len(s_rho2)
    l_s_dgdt = len(s_dgdt)
    l_s_PIMPLE, l_s_GAMGPCG, l_s_smoothSolver = len(s_PIMPLE), len(s_GAMGPCG), len(s_smoothSolver)

    #Inicialização das listas
    deltaT, time = [], []
    contErrP1max, contErrP1min, contErrP2max, contErrP2min = [], [], [], []
    rho1min, rho1max, rho2min, rho2max = [], [], [], []
    dgdtmin, dgdtmax = [], []
    PIMPLE = []
    p_rgh, S = [], []
    total_iter_S, total_iter_p = [], []

    #Strings que armazenarão linhas contendo informações retiradas do log

    curr_PIMPLE_line = ''
    curr_smoothSolver_line, curr_GAMGPCG_line = '', ''

    #Variáveis para controle dos inicios de loop

    inicio_loop_g = inicio_loop_s = True

    #Variáveis para armazenar valores mínimos e máximos

    v_contErrP1min = v_contErrP1max = None
    v_contErrP2min = v_contErrP2max = None
    v_rho1min = v_rho1max = None
    v_rho2min = v_rho2max = None
    v_dgdtmin = v_dgdtmax = None

    for line in log_file:
        if (line[0 : l_s_inicio_loop] == s_inicio_loop):
            
            currline_list = line.split(' ')
            deltaT.append(float(currline_list[-1]))

            currline_list = log_file.readline().split(' ')
            time.append(float(currline_list[-1]))

            currline = log_file.readline()
            iter_S_loop = 0
            iter_p_loop = 0

            while (currline[0 : l_s_fim_loop] != s_fim_loop and currline):

                if (currline[0:l_s_contErrP1] == s_contErrP1):
                    v_contErrP1min, v_contErrP1max = valores_min_max(currline, v_contErrP1min, v_contErrP1max)
                
                elif (currline[0:l_s_contErrP2] == s_contErrP2):
                    v_contErrP2min, v_contErrP2max = valores_min_max(currline, v_contErrP2min, v_contErrP2max)
                
                elif (currline[0:l_s_rho1] == s_rho1):
                    v_rho1min, v_rho1max = valores_min_max(currline, v_rho1min, v_rho1max)

                elif (currline[0:l_s_rho2] == s_rho2):
                    v_rho2min, v_rho2max = valores_min_max(currline, v_rho2min, v_rho2max)

                elif (currline[0:l_s_dgdt] == s_dgdt):
                    v_dgdtmin, v_dgdtmax = valores_min_max(currline, v_dgdtmin, v_dgdtmax)

                elif (currline[0:l_s_PIMPLE] == s_PIMPLE):
                    curr_PIMPLE_line = currline
                    inicio_loop_g = True
                    inicio_loop_s = True

                elif (currline[0:l_s_GAMGPCG] == s_GAMGPCG):
                    iter_p_loop += le_total_iteracoes(currline)
                    if(inicio_loop_g):
                        curr_GAMGPCG_line = currline
                        inicio_loop_g = False

                elif (currline[0:l_s_smoothSolver] == s_smoothSolver):
                    iter_S_loop += le_total_iteracoes(currline)
                    if(inicio_loop_s):
                        curr_smoothSolver_line = currline
                        inicio_loop_s = False

                currline = log_file.readline()

            if (not currline):
                break

            contErrP1min.append(v_contErrP1min) 
            contErrP1max.append(v_contErrP1max)
            contErrP2min.append(v_contErrP2min) 
            contErrP2max.append(v_contErrP2max)
            rho1min.append(v_rho1min) 
            rho1max.append(v_rho1max)
            rho2min.append(v_rho2min) 
            rho2max.append(v_rho2max)
            dgdtmin.append(v_dgdtmin) 
            dgdtmax.append(v_dgdtmax)

            v_rho1min = v_rho1max = v_rho2min = v_rho2max = v_dgdtmin = v_dgdtmax = None
            v_contErrP1min = v_contErrP1max = v_contErrP2min = v_contErrP2max = None

            trata_linha_PIMPLE(curr_PIMPLE_line, PIMPLE)
            trata_linha_GAMGPCG(curr_GAMGPCG_line, p_rgh)
            S_type = trata_linha_smoothSolver(curr_smoothSolver_line, S, S_type)

            if (iter_p_loop > 0):
                total_iter_p.append(iter_p_loop)
            if (iter_S_loop > 0):    
                total_iter_S.append(iter_S_loop)


    log_file.close()

    if (len(time) > len(PIMPLE)):
        time.pop()
        deltaT.pop()

    salvar_g = input("Deseja salvar os gráficos em arquivos png? (S)im, (N)ão: ").lower()

    while (salvar_g != 's' and salvar_g != 'n'):
        print("Entrada inválida")
        salvar_g = input("Deseja salvar os gráficos em arquivos png? (S)im, (N)ão: ").lower()

    if (salvar_g == 's'):
        dir_g = input("Diretório onde salvar os gráficos: ")
        if (dir_g[-1] != '/'):
            dir_g = dir_g + '/'
    else:
        dir_g = ''


    #Grafico 1
    gera_grafico(p_rgh, S, time, salvar_g, dir_g, "Time [s]", "Initial residual", "p_rgh", S_type, "Residuals")
    #Grafico 2
    gera_grafico(total_iter_p, total_iter_S, time, salvar_g, dir_g, "Time [s]", "No. of Iterations", "p_rgh", S_type, "Iterations")
    #Grafico 3
    gera_grafico(PIMPLE, [], time, salvar_g, dir_g, "Time [s]", "Max Iterations", "PIMPLE", None, "Max Iterations")
    #Grafico 4-1
    gera_grafico(contErrP1max, contErrP1min, time, salvar_g, dir_g, "Time [s]", "contErrP1", "contErrP1max", "contErrP1min", "Valores contErrP1")
    #Grafico 4-2
    gera_grafico(contErrP2max, contErrP2min, time, salvar_g, dir_g, "Time [s]", "contErrP2", "contErrP2max", "contErrP2min", "Valores contErrP2")
    #Grafico 4-3
    gera_grafico(rho1max, rho1min, time, salvar_g, dir_g, "Time [s]", "rho1", "rho1max", "rho1min", "Valores rho1")
    #Grafico 4-4
    gera_grafico(rho2max, rho2min, time, salvar_g, dir_g, "Time [s]", "rho2", "rho2max", "rho2min", "Valores rho2")
    #Grafico 4-5
    gera_grafico(dgdtmax, dgdtmin, time, salvar_g, dir_g, "Time [s]", "dgdt", "dgdtmax", "dgdtmin", "Valores dgdt")
    #Grafico 5
    gera_grafico(deltaT, [], time, salvar_g, dir_g, "Time [s]", "deltaT [s]", "deltaT", None, "deltaT")


#A função buscará pelas palavras "min" e "max" e extrairá os valores após elas
def trata_string_min_max(linha):

    if (not linha):
        return
    
    lista_linha = linha.split(' ')

    i_min = 0
    i_max = 0
    tam = len(lista_linha)

    while (i_min < tam and lista_linha[i_min].find("min") == -1):
        i_min += 1

    
    while (i_max < tam and lista_linha[i_max].find("max") == -1):
        i_max += 1
    
    if (i_min < tam):
        while (i_min < tam and not any(chr.isdigit() for chr in lista_linha[i_min])):
            i_min += 1
        if (i_min < tam):
            v_min = extrair_num_string(lista_linha[i_min])
    
    if (i_max < tam):
        while (i_max < tam and not any(chr.isdigit() for chr in lista_linha[i_max])):
            i_max += 1
        if (i_max < tam):
            v_max = extrair_num_string(lista_linha[i_max])

    return v_min, v_max

def valores_min_max(linha, v_min, v_max):
    v_min_aux, v_max_aux = trata_string_min_max(linha)
    if (v_min == None or v_min_aux < v_min):
        v_min = v_min_aux
    if (v_max == None or v_max_aux > v_max):
        v_max = v_max_aux
    return v_min, v_max


def trata_linha_PIMPLE(linha, PIMPLE):

    if (not linha):
        return

    lista_linha = linha.split(' ')
    for s in lista_linha:
        if (s.isdigit()):
            PIMPLE.append(int(s))
            return


def trata_linha_GAMGPCG(linha, p_rgh):

    if (not linha):
        return

    lista_linha = linha.split(' ')

    i_initial = 0
    tam = len(lista_linha)

    while (i_initial < tam and lista_linha[i_initial] == "Initial"):
        i_initial += 1

    while (i_initial < tam and not any(chr.isdigit() for chr in lista_linha[i_initial])):
        i_initial += 1

    if (i_initial < tam):
        p_rgh.append(extrair_num_string(lista_linha[i_initial]))


def trata_linha_smoothSolver(linha, S, S_type):

    if (not linha):
        return


    lista_linha = linha.split(' ')

    i_initial = 0
    tam = len(lista_linha)

    while (i_initial < tam and lista_linha[i_initial] == "Initial"):
        i_initial += 1

    while (i_initial < tam and not any(chr.isdigit() for chr in lista_linha[i_initial])):
        i_initial += 1

    if (i_initial < tam):
        S.append(extrair_num_string(lista_linha[i_initial]))


    if (not S_type):
        for word in lista_linha:
            if (word[0:2] == "S."):
                if ('z' > word[-1] and word[-1] > 'a'):
                    S_type = word
                else:
                    S_type = word[:-1]
                break

    return (S_type)

def le_total_iteracoes(linha):

    lista_linha = linha.split(' ')

    #Não contar o \n ao fim da linha
    if (lista_linha[-1][:-1].isdigit() and lista_linha[-2] == "Iterations"):
        return int(lista_linha[-1][:-1])
    
    return 0

def extrair_num_string(string):

    i = 0

    while (string[i] != '-' and not string[i].isdigit()):
        i += 1

    j = len(string) - 1

    while (not string[j].isdigit()):
        j -= 1
    
    return float(string[i:j+1])


def gera_grafico(l1, l2, time, s, dir, xlabel, ylabel, label_l1, label_l2, title):

    if (len(l1) == 0 and len(l2) == 0):
        return

    plt.clf()
    if (len(l1) > 0):
        plt.plot(time, l1, label=label_l1, linewidth="0.8")
    if (len(l2) > 0):
        plt.plot(time, l2, label=label_l2, linewidth="0.8")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend()
    plt.grid("on")
    if (s == 'n'):
        plt.show()
    else:
        plt.savefig(dir + title + ".png")

main()